/*
SQLyog Enterprise - MySQL GUI v8.02 RC
MySQL - 5.5.24-log : Database - grip
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`grip` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `grip`;

/*Table structure for table `transfer` */

DROP TABLE IF EXISTS `transfer`;

CREATE TABLE `transfer` (
  `tid` int(5) NOT NULL AUTO_INCREMENT,
  `transferfrom` int(5) DEFAULT NULL,
  `transferto` int(5) DEFAULT NULL,
  `credits` decimal(10,0) DEFAULT NULL,
  `date` varchar(60) DEFAULT NULL,
  `bywhom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

/*Data for the table `transfer` */

insert  into `transfer`(`tid`,`transferfrom`,`transferto`,`credits`,`date`,`bywhom`) values (50,3,5,'2','Mar 6, 2018 6:53:16 PM','Abc'),(51,4,5,'10','Mar 6, 2018 7:17:01 PM','fgh'),(52,2,3,'8','Mar 6, 2018 7:19:11 PM','ddd'),(53,7,3,'11','Mar 6, 2018 7:19:31 PM','uio');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `uid` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` varchar(15) NOT NULL,
  `credit` decimal(10,0) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`uid`,`name`,`email`,`gender`,`dob`,`credit`) values (1,'Karan','kj@test.com','male','26/03/18','20'),(2,'Arjun','arjun@arjun.com','male','19/09/97','12'),(3,'Kiran','surraj@kiran.com','female','01/05/99','37'),(4,'Arun','arjun@gmail.com','male','14/02/97','10'),(5,'Sahib','noname@test.com','male','17/03/98','32'),(6,'Puneet','test@test.com','male','14/05/98','20'),(7,'vidhi','vid@wid.com','female','23/11/98','9');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
